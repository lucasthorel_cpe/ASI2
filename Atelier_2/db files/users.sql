INSERT INTO public.users (id,balance,fname,lname,"password",pseudo) VALUES
	 (8,5000.0,'Test','Test','$2a$10$HnKNWT31Jugw3qHZ/9v7vu09xdARx2SmDmb03Dt87O33cHa9Q6Kgq','test'),
	 (9,5000.0,'Lucas','Thorel','$2a$10$W2HnbZQbAuhCZf./dAqxZOkNWBZcFTENlN.8PzwNAk1j2zGG3.eLS','lulu'),
	 (13,5000.0,'Lucas','Thorel','$2a$10$d6w/HV.PYcsbFyQSfSnJUemJNiEoY8.2k.2MsV22mgxZO9yte3wUq','lulull'),
	 (15,5000.0,'Tom','Echer','$2a$10$eG9uf/vZpuPG7FAsQsI1our4mvvqqBtw3sn2pzYXXSOAfzSKYcecu','tom.eshh'),
	 (44,5000.0,'po','po','$2a$10$B4PSHtZP3Ly5hSVQ.18WEOKutvV.zDtOjyRrA8nXVslH92TF0q.GC','pop'),
	 (6,0.0,'poi','po','$2a$10$oKSp1hQYmqp6kalM2nirtuP7RtdQGDCGPqKPjYlvbf1jNk3tCarBq','po'),
	 (7,75000.0,'Lucas','Thorela','$2a$10$JwxawSWnA9IebTYBhUVux.4CHvd5uZr1uHU4AvegML13gNbK2WceW','lucas.thorel@cpe.fr'),
	 (48,5000.0,'Lulu','Toto','$2a$10$jYsRsUJHZGObshxaEUykTe4fdq5SJhDRm7MoS96.bKBiibK7a3Yo6','to'),
	 (49,5000.0,'Lulu','lulu','$2a$10$ddDvB7fx7kDYdsw6NfY/B.nEMJA6V7DLGo3YIifqPEvoRgfo948Fu','lu'),
	 (50,5000.0,'l','l','$2a$10$ryJbwyCJJJa.WJTIwa2Ymu2hjyGP1hUchHeXhQjXXcB2C1ZKfC.T.','issou'),
	 (51,5000.0,'Tom','Echer','$2a$10$v4OvOYnonBlDbJnOYzifLO/BWtYg1nMMbOCRclyaJkqj6fpQydCIi','totoleBG');