package com.sp;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import com.sp.model.User;
import com.sp.service.UserService;

@Component
public class DataGenerator implements ApplicationRunner {

    private final UserService userService;

    public DataGenerator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (this.userService.getAllUser().size() != 0) {
            return;
        }
        User u = new User();
        u.setPseudo("jordan");
        u.setPassword("jordan");
        u.setBalance(500.0F);
        u.setLname("jordan");
        u.setFname("jordan");
        this.userService.addUser(u);
        System.out.println("compte créé");
    }
}
