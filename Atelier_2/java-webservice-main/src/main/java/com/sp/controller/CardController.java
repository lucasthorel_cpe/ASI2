package com.sp.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.dto.CardDTO;
import com.sp.dto.CardPostDTO;
import com.sp.dto.CodeReponseDTO;
import com.sp.dto.UserDTO;
import com.sp.mapper.CardMapper;
import com.sp.mapper.UserMapper;
import com.sp.model.Card;
import com.sp.model.User;
import com.sp.service.CardService;
import com.sp.service.UserService;


@RestController
public class CardController {
    @Autowired
    CardService	 cService;

    @Autowired
    CardMapper cMapper;
    
    @Autowired
    UserService uService;


    /**
     *
     * Récupération des informations d'une carte à partir d'un ID
     *
     */
	@RequestMapping(method=RequestMethod.GET,value="/cards/{id}")
	public CardDTO getMsg(@PathVariable String id) {
        Card c=cService.getCard(Long.parseLong(id));
        
        return cMapper.toDTO(c);
	}
	
    /**
    *
    * Récupération de l'ensemble des cartes de l'utilisateur requetant. 
    *
    */
	@RequestMapping(method=RequestMethod.GET,value="/cards")
	public List<CardDTO> getMsg(Principal principal) {
        User u=uService.getUserByPseudo(principal.getName());
		List<Card> Card = cService.getAllCardByUser(u);
		List<CardDTO> CardList = new ArrayList<CardDTO>();
		for (int i = 0; i < Card.size(); i++) {
			CardList.add(cMapper.toDTO(Card.get(i)));
		  }
		return CardList;
	}

    /**
    *
    * Création d'une carte
    *
    */
	@RequestMapping(method=RequestMethod.POST,value="/cards")
	public ResponseEntity<CodeReponseDTO> addUser(@RequestBody CardPostDTO cardDTO) {
		Card card = cMapper.PosttoModel(cardDTO);
		if(card.getTemplateCard() != null && card.getOwner() != null) {
			cService.addCard(card);
			return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 1, "successful card creation"), HttpStatus.ACCEPTED);
		}else {
			return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 0, "transaction card failed : some arguments are wrong"), HttpStatus.CONFLICT);
		}
	}

}
