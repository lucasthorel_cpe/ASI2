package com.sp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.dto.CardDTO;
import com.sp.dto.TemplateCardDTO;
import com.sp.dto.UserDTO;
import com.sp.mapper.TemplateCardMapper;
import com.sp.mapper.UserMapper;
import com.sp.model.Card;
import com.sp.model.TemplateCard;
import com.sp.service.TemplateCardService;


@RestController
public class TemplateCardController {
    @Autowired
    TemplateCardService tcService;
    @Autowired
    TemplateCardMapper tcMapper;


    /**
    *
    * Récupération des informations d'une templatecard via son ID
    *
    */
	@RequestMapping(method=RequestMethod.GET,value="/templatecard/{id}")
	public TemplateCard getMsg(@PathVariable String id) {
		TemplateCard tc= tcService.getTemplateCard(Long.parseLong(id));
        return tc;
	}

    /**
    *
    * Récupération des l'ensemble des templatecards
    *
    */	
	@RequestMapping(method=RequestMethod.GET,value="/templatecard")
	public List<TemplateCardDTO> getMsg() {
		List<TemplateCard> TemplateCard = tcService.getAllTemplateCard();
		List<TemplateCardDTO> TemplateCardList = new ArrayList<TemplateCardDTO>();
		for (int i = 0; i < TemplateCard.size(); i++) {
			TemplateCardList.add(tcMapper.toDTO(TemplateCard.get(i)));
		  }
		return TemplateCardList;
	}
    /**
    *
    * Création d'une template card
    *
    */	
	@RequestMapping(method=RequestMethod.POST,value="/templatecard")
	public void addUser2(@RequestBody TemplateCard template_card) {
		tcService.addCard(template_card);
	}

}
