package com.sp.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.dto.CodeReponseDTO;
import com.sp.dto.TemplateCardDTO;
import com.sp.dto.TransactionDTO;
import com.sp.dto.TransactionPostDTO;
import com.sp.mapper.TransactionMapper;
import com.sp.model.Card;
import com.sp.model.TemplateCard;
import com.sp.model.Transaction;
import com.sp.model.User;
import com.sp.service.CardService;
import com.sp.service.TransactionService;
import com.sp.service.UserService;


@RestController
public class TransactionController {
    @Autowired
    TransactionService tService;
    @Autowired
    UserService uService;
    @Autowired
    CardService cService;
    

    @Autowired
    TransactionMapper tMapper;

    /**
    *
    * Récupération des informations d'une transaction par son ID
    *
    */	
	@RequestMapping(method=RequestMethod.GET,value="/transactions/{id}")
	public TransactionDTO getMsg(@PathVariable String id) {
        Transaction t= tService.getTransaction(Long.parseLong(id));
        
        return tMapper.toDTO(t);
	}
	
    /**
    *
    * Récupération de l'ensemble des transactions
    *
    */	
	@RequestMapping(method=RequestMethod.GET,value="/transactions")
	public List<TransactionDTO> getMsg2(@RequestParam("status") Optional<String> status) {
		
		List<TransactionDTO> TransactionList = new ArrayList<TransactionDTO>();
		if (status.isPresent()) {
			String myStatus = status.get();
			List<Transaction> Transaction = tService.getAllTransactionActive(Long.parseLong(myStatus));
			for (int i = 0; i < Transaction.size(); i++) {
				TransactionList.add(tMapper.toDTO(Transaction.get(i)));
			  }
		}else {
			List<Transaction> Transaction = tService.getAllTransaction();
			for (int i = 0; i < Transaction.size(); i++) {
				TransactionList.add(tMapper.toDTO(Transaction.get(i)));
			  }
		}
		return TransactionList;
	}

    /**
    *
    * Création d'une transaction
    *
    */	
	@RequestMapping(method=RequestMethod.POST,value="/transactions")
	public ResponseEntity<CodeReponseDTO> addUser(@RequestBody TransactionPostDTO transactionPostDTO, Principal principal) {
        User u=uService.getUserByPseudo(principal.getName());
		transactionPostDTO.setSeller_id(u.getId());
		Transaction transaction = tMapper.PosttoModel(transactionPostDTO);
		if(tService.ifCardOnSale(transaction.getCard())) {
			return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 0, "transaction creation failed"), HttpStatus.CONFLICT);
 
		}
		if(transaction.getUserSeller() != null && transaction.getCard() != null) {
			tService.addCard(transaction);
			return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 1, "successful transaction creation"), HttpStatus.ACCEPTED);
		}else {
			return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 0, "transaction creation failed : some arguments are wrong"), HttpStatus.CONFLICT);

		}
		
	}
	
	/**
    *
    * Modification des informations de la transaction via le PUT
    *
    */	
	@RequestMapping(method=RequestMethod.PUT,value="/transactions/{id}")
	public ResponseEntity<CodeReponseDTO> addUser(@PathVariable String id, Principal principal) {
        User u=uService.getUserByPseudo(principal.getName());
        try {
        	
            Transaction transaction = tService.getTransaction(Long.parseLong(id));
            System.out.println(tService.ifTransactionIsActive(transaction));
    		if(tService.ifTransactionIsActive(transaction)) {
    			if(u.getBalance() >= transaction.getCard().getTemplateCard().getPrice()) {
    			transaction.setUserBuyer(u);
    			transaction.setStatus((long) 1);
    			transaction.setUpdateAt(new Date());
    			transaction.getUserBuyer().setBalance(transaction.getUserBuyer().getBalance()-transaction.getCard().getTemplateCard().getPrice());
    			transaction.getUserSeller().setBalance(transaction.getUserSeller().getBalance()+transaction.getCard().getTemplateCard().getPrice());
    			System.out.println(transaction.getCard().getTemplateCard().getPrice());
    			System.out.println(transaction.getUserBuyer().getBalance());
    			transaction.getCard().setOwner(u);
    			tService.addCard(transaction);
    			}else {
            		return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 0, "transaction update failed : no enought money"), HttpStatus.CONFLICT);

    			}
    		}else{
        		return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 0, "transaction update failed"), HttpStatus.CONFLICT);
    		}
        	
        }catch(Exception e ){
            System.out.println("Problème lors de l'achat");
    		return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 0, "transaction update failed"), HttpStatus.CONFLICT);

        }
		return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 1, "successful transaction update"), HttpStatus.ACCEPTED);

	}
	
	
	

}
