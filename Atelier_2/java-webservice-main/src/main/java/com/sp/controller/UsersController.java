package com.sp.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import com.sp.dto.CodeReponseDTO;
import com.sp.dto.UserDTO;
import com.sp.dto.UserPostDTO;
import com.sp.mapper.UserMapper;
import com.sp.model.User;
import com.sp.service.UserService;


@RestController
public class UsersController {
    @Autowired
    UserService uService;

    @Autowired
    UserMapper uMapper;

    /**
    *
    * Récupération des informations de l'utilisteur exécutant la requete
    *
    */	
	@RequestMapping(method=RequestMethod.GET,value="/users")
	public UserDTO getMsg(Principal principal) {
        User u=uService.getUserByPseudo(principal.getName());
        return uMapper.getUserDTO(u);
		
	}
    /**
    *
    * Récupération des informations de l'utilisteur via l'ID
    *
    */	
	@RequestMapping(method=RequestMethod.GET,value="/users/{id}")
	public UserDTO getMsg(@PathVariable String id) {
        User u=uService.getUser(Long.parseLong(id));
        return uMapper.getUserDTO(u);
	}
	
    /**
    *
    * Création d'un utilisateur via POST
    *
    */	
	@RequestMapping(method=RequestMethod.POST,value="/users")
	public ResponseEntity<CodeReponseDTO> addUser2(@RequestBody UserPostDTO user) {

		User userModel = null;
		try {
			userModel = uMapper.PosttoModel(user);
			uService.addUser(userModel);
		}catch(Exception e ){
            System.out.println("Duplication d'utilisateur");
    		return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 0, "user creation failed"), HttpStatus.CONFLICT);

        }
		
		
		return new ResponseEntity<CodeReponseDTO>(new CodeReponseDTO((long) 1, "successful user creation"), HttpStatus.ACCEPTED);
	}
	
}
