package com.sp.dto;

public class CardDTO {
	
	public TemplateCardDTO tc;

	public Long id;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CardDTO(Long id, TemplateCardDTO tc) {
		super();
		this.tc = tc;
		this.id = id;
	}

	public TemplateCardDTO getTc() {
		return tc;
	}

	public void setTc(Long id, TemplateCardDTO tc) {
		this.tc = tc;
		this.id = id;
	}
	
	
	
	
}
