package com.sp.dto;

public class CardPostDTO {
	public Long userId;
	public Long templateCardId;
	
	public CardPostDTO(Long user_id, Long template_card_id) {
		this.userId = user_id;
		this.templateCardId = template_card_id;
		System.out.println(user_id);
		System.out.println(template_card_id);
	}
	
	public CardPostDTO() {}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getTemplateCardId() {
		return templateCardId;
	}
	public void setTemplateCardId(Long templateCardId) {
		this.templateCardId = templateCardId;
	}
	
	
}
