package com.sp.dto;

import java.io.Serializable;

public class CodeReponseDTO implements Serializable {


	private final Long status;
	private final String message;

	public CodeReponseDTO(Long status, String message) {
		this.status = status;
		this.message = message;
	}

	public Long getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

}