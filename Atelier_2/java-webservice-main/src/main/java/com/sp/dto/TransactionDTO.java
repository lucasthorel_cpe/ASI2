package com.sp.dto;

import java.util.Date;

import javax.persistence.ManyToOne;

import com.sp.model.User;

public class TransactionDTO {
    private Long id;
	public UserDTO userBuyer;
	public UserDTO userSeller;
	public CardDTO card;
	public Date updateAt;
	public Long status;
	public TransactionDTO(Long id, UserDTO userBuyer, UserDTO userSeller, CardDTO card, Date updateAt, Long status) {
		super();
		this.id = id;
		this.userBuyer = userBuyer;
		this.userSeller = userSeller;
		this.card = card;
		this.updateAt = updateAt;
		this.status = status;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public UserDTO getUserBuyer() {
		return userBuyer;
	}
	public void setUserBuyer(UserDTO userBuyer) {
		this.userBuyer = userBuyer;
	}
	public UserDTO getUserSeller() {
		return userSeller;
	}
	public void setUserSeller(UserDTO userSeller) {
		this.userSeller = userSeller;
	}
	public CardDTO getCard() {
		return card;
	}
	public void setCard(CardDTO card) {
		this.card = card;
	}
	public Date getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	
	
}
