package com.sp.mapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.dto.CardDTO;
import com.sp.dto.CardPostDTO;
import com.sp.dto.TemplateCardDTO;
import com.sp.model.Card;
import com.sp.model.TemplateCard;
import com.sp.model.User;
import com.sp.service.TemplateCardService;
import com.sp.service.UserService;

@Component
public class CardMapper {
	@Autowired
	UserService us;
	@Autowired
	TemplateCardService ts;

	@Autowired
	TemplateCardMapper tcm;

	public CardDTO toDTO(Card card){
		TemplateCardDTO tcDTO = tcm.toDTO(card.getTemplateCard());
		return new CardDTO(card.getId(), tcDTO);
		
	}
	
	public Card PosttoModel(CardPostDTO card){
		TemplateCard tc = ts.getTemplateCard(card.getTemplateCardId());
		User u = us.getUser(card.getUserId());
		return new Card(u,tc);
		
	}
}
