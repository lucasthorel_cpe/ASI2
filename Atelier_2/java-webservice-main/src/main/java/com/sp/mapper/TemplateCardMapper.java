package com.sp.mapper;

import java.util.List;

import org.springframework.stereotype.Component;
import com.sp.dto.TemplateCardDTO;
import com.sp.model.TemplateCard;

@Component
public class TemplateCardMapper {
	public TemplateCardDTO toDTO(TemplateCard tc){

		return new TemplateCardDTO(tc.getName(), tc.getDescription(), tc.getFamily(),tc.getAffinity(),tc.getHp(),tc.getAttack(), tc.getDefense(), tc.getEnergy(), tc.getPrice(), tc.getAvatarURL());

	}
}
