package com.sp.mapper;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.dto.CardDTO;
import com.sp.dto.CardPostDTO;
import com.sp.dto.TemplateCardDTO;
import com.sp.dto.TransactionDTO;
import com.sp.dto.TransactionPostDTO;
import com.sp.dto.UserDTO;
import com.sp.model.Card;
import com.sp.model.TemplateCard;
import com.sp.model.Transaction;
import com.sp.model.User;
import com.sp.service.CardService;
import com.sp.service.TemplateCardService;
import com.sp.service.UserService;

@Component
public class TransactionMapper {

	@Autowired
	TransactionMapper tm;
	

	@Autowired
	UserMapper um;
	

	@Autowired
	CardMapper cm;
	

	@Autowired
	CardService cs;
	

	@Autowired
	UserService us;

	public TransactionDTO toDTO(Transaction transaction){
		UserDTO Buyer = null;
		UserDTO Seller = um.getUserDTO(transaction.getUserSeller());
		if(transaction.getUserBuyer() != null) {
			Buyer = um.getUserDTO(transaction.getUserBuyer());
		}
		CardDTO Card = cm.toDTO(transaction.getCard());
		return new TransactionDTO(transaction.getId(), Buyer, Seller, Card, transaction.getUpdateAt(), transaction.getStatus());
		
	}
	
	public Transaction PosttoModel(TransactionPostDTO transaction){
		User seller = us.getUser(transaction.getSeller_id());
		User buyer = null;
		Card card = cs.getCard(transaction.getCard_id());
		return new Transaction(buyer,seller,card,transaction.getUpdateAt(),transaction.getStatus());
		
	}
	

}
