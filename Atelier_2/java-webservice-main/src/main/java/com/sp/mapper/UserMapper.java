package com.sp.mapper;

import java.util.List;

import org.springframework.stereotype.Component;
import com.sp.dto.UserDTO;
import com.sp.dto.UserPostDTO;
import com.sp.model.User;

@Component
public class UserMapper {

	public UserDTO getUserDTO(User user){

		return new UserDTO(user.getLname(),user.getFname(),user.getPseudo(), user.getBalance());
		
	}
	
	public User PosttoModel(UserPostDTO user){

		return new User(user.getPseudo(),user.getPassword(),user.getLname(), user.getFname(), (float) 5000);
		
	}
}
