package com.sp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Cards")
public class Card {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    private Long id;
	
	
	@ManyToOne()
    public User owner;
    
	
	@ManyToOne()
	public TemplateCard templateCard;
	
	public Card() {
	}
	
	public Card(User user, TemplateCard templateCard) {
		this.owner = user;
		this.templateCard = templateCard;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id= id;
	}
	
	public User getOwner() {
		return owner;
	}
	public void setOwner(User user) {
		this.owner=user;
	}
	
	public TemplateCard getTemplateCard() {
		return templateCard;
	}
	public void setTemplateCard(TemplateCard templateCard) {
		this.templateCard=templateCard;
	}

	
}
