package com.sp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Transactions")
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    private Long id;
	
	@ManyToOne()
	public User userBuyer;
	
	@ManyToOne()
	public User userSeller;
	

	@ManyToOne()
	public Card card;

	public Date updateAt;
	
	public Long status;

	public Transaction(User userBuyer, User userSeller, Card card, Date updateAt, Long status) {
		this.id = id;
		this.userBuyer = userBuyer;
		this.userSeller = userSeller;
		this.card = card;
		this.updateAt = updateAt;
		this.status = status;
	}

	public Transaction() {}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUserBuyer() {
		return userBuyer;
	}

	public void setUserBuyer(User userBuyer) {
		this.userBuyer = userBuyer;
	}

	public User getUserSeller() {
		return userSeller;
	}

	public void setUserSeller(User userSeller) {
		this.userSeller = userSeller;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}
	
}
