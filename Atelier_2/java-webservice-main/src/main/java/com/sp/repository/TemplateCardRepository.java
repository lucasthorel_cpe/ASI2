package com.sp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import com.sp.model.TemplateCard;
import org.springframework.stereotype.Service;

import org.springframework.data.jpa.repository.Query;

public interface TemplateCardRepository extends CrudRepository<TemplateCard, Integer> {

	public List<TemplateCard> findAll();
	public Optional<TemplateCard> findById(Long id);
	
	@Query(value = "SELECT id, affinity, attack, avatarurl, defense, description, energy, family, hp, name, price FROM public.templatecards ORDER BY RANDOM() LIMIT 5", nativeQuery = true)
	List<TemplateCard> FindFiveCardRandom();
}
