package com.sp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.sp.model.Card;
import com.sp.model.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

	public List<Transaction> findAll();
	public List<Transaction> findByStatus(Long status);
	public Optional<Transaction> findById(Long id);
	public Optional<Transaction> findByIdAndStatus(Long id, Long status);

	public Optional<Transaction> findByStatusAndCard(Long status, Card card);
	
}
