package com.sp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.Card;
import com.sp.model.User;
import com.sp.repository.CardRepository;

@Service
public class CardService {
	@Autowired
	CardRepository cRepository;
	public void addCard(Card c) {
		cRepository.save(c);
	}
	
    /**
    *
    * Récupération des cartes par une ID
    *
    */	
	public Card getCard(Long id) {
		Optional<Card> cOpt =cRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}
	
    /**
    *
    * Récupération de l'ensemble des cartes
    *
    */	
	public List<Card> getAllCard() {
		List<Card> cards = cRepository.findAll();
		return cards;
	}
	

    /**
    *
    * Récupération de l'ensemble des cartes d'un utilisateur
    *
    */	
	public List<Card> getAllCardByUser(User user) {
		List<Card> cards = cRepository.findByOwner(user);
		return cards;
	}

}
