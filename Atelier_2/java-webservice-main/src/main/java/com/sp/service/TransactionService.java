package com.sp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.Card;
import com.sp.model.Transaction;
import com.sp.repository.TransactionRepository;

@Service
public class TransactionService {
	@Autowired
	TransactionRepository tRepository;
	
	
	
    /**
    *
    * Création d'une carte
    *
    */	
	public void addCard(Transaction tc) {
		Transaction createdTransaction= tRepository.save(tc);
		System.out.println(createdTransaction);
	}
	
    /**
    *
    * Récupération d'une carte à partir d'un ID
    *
    */		
	public Transaction getTransaction(Long id) {
		Optional<Transaction> cOpt = tRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}

    /**
    *
    * Récupération de l'ensemble des cartes
    *
    */	
	public List<Transaction> getAllTransaction() {
		List<Transaction> transactions = tRepository.findAll();
		return transactions;
	}

    /**
    *
    * Récupération de l'ensemble des cartes en fonction d'un status
    *
    */		
	public List<Transaction> getAllTransactionActive(Long status) {
		List<Transaction> transactions = tRepository.findByStatus(status);
		return transactions;
	}

    /**
    *
    * Vérifie si la carte est déjà en vente
    *
    */	
	public Boolean ifCardOnSale(Card card) {
		Optional<Transaction> cOpt = tRepository.findByStatusAndCard((long) 0,card);
		if (cOpt.isPresent()) {
			return true;
		}else {
			return false;
		}
	}
	
    /**
    *
    * Vérifie si la transaction est toujours d'activité
    *
    */		
	public Boolean ifTransactionIsActive(Transaction transaction) {
		Optional<Transaction> cOpt = tRepository.findByIdAndStatus(transaction.getId(), (long) 0);
		if (cOpt.isPresent()) {
			return true;
		}else {
			return false;
		}
		
	}
	
	
}
