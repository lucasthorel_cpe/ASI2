package com.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.sp.model.Card;
import com.sp.model.TemplateCard;
import com.sp.model.User;
import com.sp.repository.CardRepository;
import com.sp.repository.TemplateCardRepository;
import com.sp.repository.UserRepository;

@Service
public class UserService implements UserDetailsService{
	@Autowired
	UserRepository uRepository;
	

	@Autowired
	TemplateCardRepository tcRepository;

	@Autowired
	CardRepository cRepository;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;
	
    /**
    *
    * Création d'un utilisateur
    *
    */	
	public void addUser(User u) {
		u.setPassword(bcryptEncoder.encode(u.getPassword()));
		User createdUser= uRepository.save(u);
		List<TemplateCard> templatecard = tcRepository.FindFiveCardRandom();

		for (int i = 0; i < templatecard.size(); i++) {
			Card card = new Card();
			card.setOwner(u);
			card.setTemplateCard(templatecard.get(i));
			cRepository.save(card);
		}
		
		System.out.println(createdUser);
	}
	
    /**
    *
    * Récupération des informations utilisateurs à partir de son ID
    *
    */	
	public User getUser(Long id) {
		Optional<User> uOpt =uRepository.findById(id);
		if (uOpt.isPresent()) {
			return uOpt.get();
		}else {
			return null;
		}
	}
	
    /**
    *
    * Récupération des informations utilisateurs à partir de son Pseudo
    *
    */	
	public User getUserByPseudo(String pseudo){
		User user = uRepository.findByPseudo(pseudo);
		return user;
	}

	
    /**
    *
    * Récupération de l'ensemble des utilisateurs
    *
    */	
	public List<User> getAllUser() {
		List<User> user = uRepository.findAll();
		return user;
	}
	

	
    /**
    *
    * Connexion d'un utilisateur
    *
    */		
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = uRepository.findByPseudo(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getPseudo(), user.getPassword(),
				new ArrayList<>());
	}

}
