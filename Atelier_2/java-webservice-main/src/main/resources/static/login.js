function send(event) {
    event.preventDefault()

    var user = {
        username:$("#pseudo").val(),
        password:$("#password").val(),
    }

    $.ajax({
        url: 'http://127.0.0.1:8080/users/login',
        type: "POST",
        dataType: 'json',
        data: JSON.stringify(user),
        contentType: 'application/json;charset=UTF-8', 
        success: function(data) {
            localStorage.setItem('bearer', data.token);
            window.location.href = "./sell.html";
        },
        error: function(errorThrown){
            alert("An error occured \r\n" + errorThrown.status + " " +   errorThrown.message);
        }
    })
    
}