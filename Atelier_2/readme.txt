Les membres du groupes :
	- Tom ECHER
	- Lucas THOREL

Les éléments réalisés du cahier des charges :
	- L'utilisateur est capable de s'authentifier sur le site
		/login.html pour se connecter
		/register.html pour s'enregistrer (il reçoit 5 cartes alétatoires à la création de son compte)
	- L'utilisateur est redirigé sur la page de connexion si il tente d'accéder à une page nécessitant une connexion.
	- L'utilisateur accède après son authentification à la page d'accueil ou il choisira entre vendre, acheter et jouer (pas encore disponible)
		/home pour la page d'accueil
		/sell.html pour voir tout son inventaire et vendre une carte
		/buy.html pour voir toutes les cartes disponibles sur le marché. Il est possible d'acheter des cartes. (Le vendeur reçoit son argent et l'acheteur en perd après la transaction).
		
Les éléments non-réalisés du cahier des charges :
	-

Des éléments éventuels réalisés en plus du cahier des charges
	- Mise en place Se SpringBoot Security pour les tokens JWT

Lien dépôt GitLab :
	- https://gitlab.com/lucasthorel_cpe/asi2

Import de la db. Après le lancement Java, vous pouvez importer un jeu de données déjà existant.